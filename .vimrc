set nocompatible

filetype off
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
Bundle 'gmarik/vundle'

" original repos on github
Bundle 'tpope/vim-fugitive'
Bundle 'mileszs/ack.vim'
Bundle 'sjl/gundo.vim'
Bundle 'fs111/pydoc.vim'
Bundle 'scrooloose/nerdtree'
Bundle 'scrooloose/syntastic'
Bundle 'scrooloose/nerdcommenter'
Bundle 'docunext/closetag.vim'
Bundle 'jmcantrell/vim-virtualenv'
Bundle 'Lokaltog/vim-powerline'
Bundle 'Raimondi/delimitMate'
Bundle 'kien/ctrlp.vim'
Bundle 'airblade/vim-gitgutter'

Bundle 'git://github.com/davidhalter/jedi-vim'

" vim-scripts repos
Bundle 'taglist.vim'

syntax on
syntax enable

set t_Co=256
set ttyfast
filetype plugin on
filetype plugin indent on

if version >= 703
   set undodir=~/.vim-undos//
   set undofile
   set undolevels=1000
   set undoreload=10000
endif

set hidden
set modeline
let mapleader = ","
set encoding=utf-8
set ffs=unix,dos,mac "Default file types
set splitright
set iskeyword+=.
set mouse=a

" Set backup to a location
set backup
set noswapfile
set backupdir=~/.vim-backups//,~/tmp/vim//,/tmp/vim//

" Set spellcheck
set spelllang=en_us

let python_highlight_all=1
let python_slow_sync=1

" Deactivate quickfix use with pyflakes
let g:pyflakes_use_quickfix = 0

set viminfo=%,'30,/100,:200,<500,f1 "save history
    "% -> save and restore buffer list
    "'30 -> save marks for this many files
    "/200 -> number of lines from search history
    ":200 -> number of lines from command line history
    "<700 -> how many lines are saved for each register
    "f1 -> save global marks

set history=800
set undolevels=800

" Filetype specific settings
au BufRead,BufNewFile *.py  set ai sw=4 sts=4 et tw=72 " Doc strs
au BufRead,BufNewFile *.js  set ai sw=2 sts=2 et tw=72 " Doc strs
au BufRead,BufNewFile *.css  set ai sw=2 sts=2 et tw=72 " Doc strs
au BufRead,BufNewFile *.html set ai sw=2 sts=2 et tw=72 " Doc strs
au BufRead,BufNewFile *.json set ai sw=4 sts=4 et tw=72 " Doc strs
au BufNewFile *.html,*.css,*.py,*.pyw,*.c,*.h,*.json set fileformat=unix
au BufRead,BufNewFile *.json setfiletype json

"""" Searching and Patterns
set ignorecase                          " search is case insensitive
set smartcase                           " search case sensitive if caps on
set incsearch                           " show best match so far
set hlsearch                            " Highlight matches to the search

"""" Display
set background=dark                     " I use dark background
set lazyredraw                          " Don't repaint when scripts are running
set scrolloff=3                         " Keep 3 lines below and above the cursor
set ruler                               " line numbers and column the cursor is on
"set number                             " Show line numbering
"set numberwidth=2                      " Use 1 col + 1 space for numbers

"let g:jellybeans_termcolors=233
colorscheme jellybeans

" Custom Higlights
hi BadWhitespace ctermbg=red guibg=red
hi ColorColumn ctermbg=black
if exists('+colorcolumn')
  au BufWinEnter *.py set colorcolumn=80
else
  au BufWinEnter *.py let w:m2=matchadd('ColorColumn', '\%>80v.\+', -1)
endif

" Match any spaces at the end of a line, or any tabs anywhere
au BufEnter,BufRead,BufNewFile,InsertEnter,InsertLeave *.py match BadWhitespace /\s\+$\|\t/

augroup HiglightTODO
	autocmd!
    autocmd WinEnter,VimEnter * :silent! call matchadd('Todo', 'TODO\|FIXME')
augroup END

" Tab labels show the filename without path(tail)
set guitablabel=%N/\ %t\ %M

"""" Messages, Info, Status
set shortmess+=a                        " Use [+] [RO] [w] for modified, read-only, modified
set showcmd                             " Display what command is waiting for an operator
set laststatus=2                        " Always show statusline, even if only 1 window
set report=0                            " Notify me whenever any lines have changed
set confirm                             " Y-N-C prompt if closing with unsaved changes
set vb t_vb=                            " Disable visual bell
set statusline=%<%f%m%r%y%=%b\ 0x%B\ \ %l,%c%V\ %P
set laststatus=2  " always a status line

"""" Editing
set backspace=2
set matchtime=2
set formatoptions-=tc
set nosmartindent

" Set autoindent
set cindent
set tabstop=4                           " Tab stop of 4
set shiftwidth=4                        " sw 4 spaces (used on auto indent)
set softtabstop=4                       " 4 spaces as a tab for bs/del
set matchpairs+=<:>                     " specially for html

"""" Coding
set showfulltag                         " Show more information while completing tags

""""" Folding
set foldmethod=indent                   " By default, use indent to determine folds
set foldlevelstart=99                   " All folds open by default
set nofoldenable

"""" Command Line
set wildmenu                            " Autocomplete features in the status bar
set wildmode=longest:full,list
set wildignore=*.o,*.obj,*.bak,*.exe,*.py[co],*.swp,*~,*.pyc,.svn

" Toggle paste mode
nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode

" New tab, cycle tabs
nmap <silent> <F5> :tabnew<CR>
nmap <silent> <F6> :tabp<CR>
nmap <silent> <F7> :tabn<CR>

""" Windows
if exists(":tab")                       " Try to move to other windows if changing buf
	set switchbuf=useopen,usetab
else                                    " Try other windows & tabs if available
    set switchbuf=useopen
endif

nmap <leader>a :Ack<Space>
map <leader>nn :tabedit ~/Dropbox/notes/

" Python Pep8-Helpers
nmap <F9> :%s/\zs\(,\\|:\)\ze\S/\1 /gc<CR>
map <s-F9> :%s/\( """*\\|"""*\s\)/"""/gc<CR>
map <s-F8> :Errors<cr>

" Copy and paste
map <C-C> "+y
map <C-V> "+p
map <C-X> "+x

" Switch buffers
map <S-TAB> :bprev<CR>
map <TAB> :bnext<CR>

" <space> toggles folds opened and closed
nnoremap <space> za

" <space> in visual mode creates a fold over the marked range
vnoremap <space> zf

map <s-Up> :cp<cr>
map <s-Down> :cn<cr>
map <s-Left> :lprev<cr>
map <s-Right> :lnext<cr>

" Just subsitute ESC with ,
map <leader> ,

" Show registers
nmap <leader>r :registers<CR>

nmap <leader>l :set list!<CR>
set listchars=tab:▸\ ,eol:¬,trail:~

set clipboard=unnamed

" map sort function to a key
vnoremap <Leader>s :sort<CR>

"""" Autocommands
:autocmd InsertEnter,InsertLeave * set cul!

if has("autocmd")
augroup vimrcEx
au!
    " In plain-text files and svn commit buffers, wrap automatically at 78 chars
    au FileType text setlocal tw=78 fo+=t

    " In all files, try to jump back to the last spot cursor was in before exiting
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \   exe "normal g`\"" |
        \ endif

    " Smart indenting for python
    au FileType python set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class,with
    " Convert tabs to spaces in python files
    au FileType python set et|retab
    set iskeyword+=.,_,$,@,%,#

    " Setup file type for snipmate
    "-----------------------------
    "au FileType python set ft=python.django
    au FileType xhtml set ft=htmldjango.html
    au FileType html set ft=htmldjango.html
    au FileType css set noexpandtab

    " Kill calltip window if we move cursor or leave insert mode
    au CursorMovedI * if pumvisible() == 0|pclose|endif
    au InsertLeave * if pumvisible() == 0|pclose|endif

    au FileType python set omnifunc=pythoncomplete#Complete
    au FileType python.django set omnifunc=pythoncomplete#Complete
    au FileType javascript set omnifunc=javascriptcomplete#CompleteJS
    au FileType html set omnifunc=htmlcomplete#CompleteTags
    au FileType htmldjango.html set omnifunc=htmlcomplete#CompleteTags
    au FileType css set omnifunc=csscomplete#CompleteCSS
    augroup END
endif

""" PLUGINS """
"""""""""""""""

" CTRL-P
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_follow_symlinks = 2
map <F1> :CtrlPMixed<CR>
nmap <leader>t :CtrlP<cr>

" NERDtree plugin settings
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']
let NERDTreeBookmarksFile="~/.vim-bookmarks/bookmarks"

" Fugitive
autocmd QuickFixCmdPost *grep* cwindow
autocmd QuickFixCmdPost *log* cwindow

" Syntastic
let g:syntastic_enable_signs=0
" when set to 2 the error window will be automatically closed when no errors
" are detected, but not opened automatically
let g:syntastic_auto_loc_list=2
let g:syntastic_auto_jump=0
let g:syntastic_check_on_open = 1
let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]'
let g:syntastic_python_flake8_post_args='--ignore=E501,E128,E127,E123,W404'
let g:syntastic_python_checker="flake8"
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_mode_map = { 'mode': 'active',
\ 'active_filetypes' : [],
\ 'passive_filetypes' : ['html', 'css'] }

" DelimitMate
let g:delimitMate_smart_quotes=0
let g:delimitMate_offByDefault=1

" Powerline
"set guifont=Consolas\ for\ Powerline\ FixedD:h9
let g:Powerline_symbols="fancy"

" Supertab
let g:SuperTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

" Snippets
au filetype python :iabbrev ipdb import ipdb;ipdb.set_trace()
au filetype python :iabbrev coding # -*- coding: utf-8 -*-<CR>

" Gitgutter
nmap <leader><PageDown> :GitGutterNextHunk<CR>
nmap <leader><PageUp> :GitGutterPrevHunk<CR>

" Toggle Gundo window
nnoremap <F3> :GundoToggle<CR>

" NerdTree Toggle
nmap <F8> :NERDTreeToggle<CR>

" CloseTag
let g:closetag_html_style=0
au FileType html,htmldjango let b:closetag_html_style=1
au FileType html,xhtml,xml,htmldjango source ~/.vim/bundle/closetag/plugin/closetag.vim
