Installation
============

Clone and automatically checkout submodules (plugins)::

  cd ~
  git clone https://bitbucket.org/arie/vimconfig.git --recursive
  mkdir -p {.vim-undos,.vim-backups,.vim-bookmarks}
  ln -sf vimconfig/.vimrc
  ln -sf vimconfig/.vim

Make sure ``ack-grep`` and ``flake8`` are installed::

  sudo apt-get install ack-grep flake8

Jedi-Vim (Python autocompletion) needs::

  pip install -U jedi

Plugins are managed via Vundle: https://github.com/gmarik/vundle

Useful articles
===============

VIM and Python
--------------

* http://mirnazim.org/writings/vim-plugins-i-use
* http://sontek.net/turning-vim-into-a-modern-python-ide

Plugins
=======

Syntastic
---------

* http://blog.thomasupton.com/2012/05/syntastic

Install node.js and npm and install some libs for syntastic syntax checker (http://mg.pov.lt/vim/doc/syntastic.txt)

* http://csslint.net/about.html
* https://github.com/jshint/node-jshint#readme

Powerline
---------
Patched Consolas font for Powerline:

* http://codejury.com/consolas-font-in-vim-powerline-windows/
